const path = require("path");
const csvFilePath='./src/data/matches.csv';
const csvFilePath1='./src/data/deliveries.csv';
const totalmatchesPerYear=require(path.join(__dirname,'./src/server/totalmatchesPerYear.cjs'));
const matchesWonPerTeamPerYear=require(path.join(__dirname,'./src/server/matchesWonPerTeamPerYear.cjs'));
const extraRunsPerTeam=require(path.join(__dirname,'./src/server/extraRunsPerTeam.cjs'));
const topTenBowlers=require(path.join(__dirname,'./src/server/topTenBowlers.cjs'));
const teamsWonTossandMatch=require(path.join(__dirname,'./src/server/teamsWonTossandMatch.cjs'));
const playerOfTheMatchPerYear=require(path.join(__dirname,'./src/server/playerOfTheMatchPerYear.cjs'));
const strikeRatePerPlayer=require(path.join(__dirname,'./src/server/strikeRatePerPlayer.cjs'));
const maximumPlayerDismissed=require(path.join(__dirname,'./src/server/maximumPlayerDismissed.cjs'));
const bestSuperoverBowler=require(path.join(__dirname,'./src/server/topSuperOverBowler.cjs'));
const csv=require('csvtojson');
const fs = require("fs");

let copyofIplMatches=[];
csv()
.fromFile(csvFilePath)
.then((iplMatches)=>{
    
copyofIplMatches=[...iplMatches];


//Number of matches played per year for all the years in IPL.

    let totalmatchesperyear=totalmatchesPerYear(iplMatches);
    fs.writeFileSync(path.join(__dirname, './src/public/output/totalmatchesPerYear.json'), JSON.stringify(totalmatchesperyear), "utf-8")

    //Number of matches won per team per year in IPL.
    let matcheswonperteamperyear=matchesWonPerTeamPerYear(iplMatches.filter(matches => matches.winner !== ""));
    fs.writeFileSync(path.join(__dirname, './src/public/output/matchesWonPerTeamPerYear.json'), JSON.stringify(matcheswonperteamperyear), "utf-8")



   // Find the number of times each team won the toss and also won the match
let teamswontossandmatch=teamsWonTossandMatch(iplMatches);
fs.writeFileSync(path.join(__dirname, './src/public/output/teamsWonTossandMatch.json'), JSON.stringify(teamswontossandmatch), "utf-8")




//Find a player who has won the highest number of Player of the Match awards for each season
let playerofthematchperyear=playerOfTheMatchPerYear(iplMatches);
fs.writeFileSync(path.join(__dirname, './src/public/output/playerOfTheMatchPerYear.json'), JSON.stringify(playerofthematchperyear), "utf-8")





})

csv()
.fromFile(csvFilePath1)
.then((playerRuns)=>{

    //Extra runs conceded per team in the year 2016
   let extrarunsperteam=extraRunsPerTeam(copyofIplMatches,playerRuns);
fs.writeFileSync(path.join(__dirname, './src/public/output/extraRunsPerTeam.json'), JSON.stringify(extrarunsperteam), "utf-8")




//Top 10 economical bowlers in the year 2015
let toptenbowlers=topTenBowlers(copyofIplMatches,playerRuns);
fs.writeFileSync(path.join(__dirname, './src/public/output/topTenBowlers.json'), JSON.stringify(toptenbowlers), "utf-8")



//Find the strike rate of a batsman for each season
let strikerate=strikeRatePerPlayer(copyofIplMatches,playerRuns);
fs.writeFileSync(path.join(__dirname, './src/public/output/strikeRatePerPlayer.json'), JSON.stringify(strikerate), "utf-8")



//Find the highest number of times one player has been dismissed by another player
let maximumdismissed=maximumPlayerDismissed(playerRuns);
fs.writeFileSync(path.join(__dirname, './src/public/output/maximumPlayerDismissed.json'), JSON.stringify(maximumdismissed), "utf-8")



//Find the bowler with the best economy in super overs
let bestBowler=bestSuperoverBowler(copyofIplMatches,playerRuns);
fs.writeFileSync(path.join(__dirname, './src/public/output/bestSuperoverBowler.json'), JSON.stringify(bestBowler), "utf-8")


})