function bestSuperoverBowler(iplMatches, playerRuns) {
  let matchid = iplMatches.reduce((acc, cur) => {
    if (cur.result === "tie") {
      acc[cur.id] = cur.id;
    }
    return acc;
  }, {});

  let numberofruns = {};
  let superoverBowlers = playerRuns.reduce((acc, cur) => {
    if (matchid[cur.match_id] && cur.inning != 1 && cur.inning != 2) {
      if (acc[cur.bowler]) {
        numberofruns[cur.bowler] += parseInt(cur.total_runs);
        acc[cur.bowler]++;
      } else {
        numberofruns[cur.bowler] = parseInt(cur.total_runs);
        acc[cur.bowler] = 1;
      }
    }
    return acc;
  }, {});

  let economical = {};
  for (let val in superoverBowlers) {
    economical[val] = numberofruns[val] / superoverBowlers[val];
  }

  let economicalaarray = Object.entries(economical);
  let sortedarray = economicalaarray.sort((first, second) => {
    return second[1] - first[1];
  });
  let finalarray = sortedarray.reverse().slice(0, 1);

  let answerObject = Object.fromEntries(finalarray);
  return answerObject;
}
module.exports = bestSuperoverBowler;
