function matchesWonPerTeamPerYear(iplMatches) {
  let matchesPereachYear = iplMatches.reduce((acc, cur) => {
 
    if (acc[cur.season]) {
      
      let valueofthisSeason = acc[cur.season];
      if (valueofthisSeason[cur.winner] ) {
        valueofthisSeason[cur.winner]++;
      } else {
        valueofthisSeason[cur.winner] = 1;
      }
    } else if(cur.winner !== undefined){
      let winningTeam = {};
      winningTeam[cur.winner] = 1;
      acc[cur.season] = winningTeam;
    }
    return acc;
  }, {});

  return matchesPereachYear;
}
module.exports = matchesWonPerTeamPerYear;
