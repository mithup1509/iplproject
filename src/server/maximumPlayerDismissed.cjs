function maximumPlayerDismissed(playerRuns) {
  let playersandbowler = playerRuns.reduce((acc, cur) => {
    if (acc[cur.player_dismissed]) {
      let valueofthisplayer = acc[cur.player_dismissed];
      if (valueofthisplayer[cur.bowler]) {
        valueofthisplayer[cur.bowler]++;
      } else {
        valueofthisplayer[cur.bowler] = 1;
      }
    } else {
      let bowler = {};
      bowler[cur.bowler] = 1;
      acc[cur.player_dismissed] = bowler;
    }
    return acc;
  }, {});

  let newArray = [];

  let arrayplayerandbowler = Object.entries(playersandbowler);
  for (let val = 1; val < arrayplayerandbowler.length; val++) {
    let value = Object.entries(arrayplayerandbowler[val][1]);

    let arrayforsort = value.sort((firstelement, secondelement) => {
      return secondelement[1] - firstelement[1];
    });
    arrayforsort.reverse();
    let arrayfor = arrayforsort[arrayforsort.length - 1];

    let temp = arrayfor[0];
    arrayfor[0] = arrayfor[1];
    arrayfor[1] = temp;
    newArray.push([arrayfor, arrayplayerandbowler[val][0]]);
  }

  newArray.sort();
  let answerObject = {};
  let obj = {};
  let final = newArray[newArray.length - 1];

  obj[final[0][1]] = final[0][0];
  answerObject[final[1]] = obj;

  return answerObject;
}
module.exports = maximumPlayerDismissed;
