function extraRunsPerTeam(iplmatches, playerRuns, year = 2016) {
  let dataofthisYear = iplmatches.reduce((acc, cur) => {
    if (cur.season == year) {
      acc[cur.id] = cur.id;
    }
    return acc;
  }, {});

  let extrarunsperteam = playerRuns.reduce((acc, cur) => {
    if (dataofthisYear[cur.match_id]) {
      if (acc[cur.bowling_team]) {
        acc[cur.bowling_team] += parseInt(cur.extra_runs);
      } else {
        acc[cur.bowling_team] = parseInt(cur.extra_runs);
      }
    }
    return acc;
  }, {});

  return extrarunsperteam;
}
module.exports = extraRunsPerTeam;
