function teamsWonTossandMatch(iplMatches){
    let teamwonbothtossmatch=iplMatches.reduce((acc,cur) => {
        if(cur.toss_winner === cur.winner){
            if(acc[cur.winner]){
                acc[cur.winner]++;
            }else{
                acc[cur.winner]=1;
            }
        }
        return acc;
    },{});
    
    return teamwonbothtossmatch;
}
module.exports=teamsWonTossandMatch;