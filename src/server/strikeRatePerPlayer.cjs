function strikeRatePerPlayer(iplMatches, playerRuns) {
  let yearandid = iplMatches.reduce((acc, cur) => {
    acc[cur.id] = cur.season;
    return acc;
  }, {});

  let numberofballs = playerRuns.reduce((acc, cur) => {
    if (yearandid[cur.match_id]) {
      if (acc[yearandid[cur.match_id]]) {
        let yeardata = acc[yearandid[cur.match_id]];
        if (yeardata[cur.batsman]) {
          yeardata[cur.batsman]++;
        } else {
          yeardata[cur.batsman] = 1;
        }
      } else {
        let players = {};
        players[cur.batsman] = 1;
        acc[yearandid[cur.match_id]] = players;
      }
    }
    return acc;
  }, {});

  let playerScores = playerRuns.reduce((acc, cur) => {
    if (yearandid[cur.match_id]) {
      if (acc[yearandid[cur.match_id]]) {
        let yeardata = acc[yearandid[cur.match_id]];
        if (yeardata[cur.batsman]) {
          yeardata[cur.batsman] += parseInt(cur.batsman_runs);
        } else {
          yeardata[cur.batsman] = parseInt(cur.batsman_runs);
        }
      } else {
        let players = {};
        players[cur.batsman] = parseInt(cur.batsman_runs);
        acc[yearandid[cur.match_id]] = players;
      }
    }
    return acc;
  }, {});
  let average = {};

  for (let val in playerScores) {
    let value = playerScores[val];
    let obj = Object.keys(value).reduce((acc, cur) => {
      acc[cur] = (playerScores[val][cur] / numberofballs[val][cur]) * 100;
      return acc;
    }, {});
    average[val] = obj;
  }
  
  return average;
}
module.exports = strikeRatePerPlayer;
