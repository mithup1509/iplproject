function matchesPerYearPerTeam(iplMatches) {
  let matchesperYear = iplMatches
    .filter((matches) => matches.season != "")
    .reduce((acc, cur) => {
      if (acc[cur.season]) {
        acc[cur.season]++;
      } else {
        acc[cur.season] = 1;
      }
      return acc;
    }, {});

  return matchesperYear;
}
module.exports = matchesPerYearPerTeam;
