function topTenBowlers(iplMatches, playerRuns, year = 2015) {
  let mathidthisYear = iplMatches.reduce((acc, cur) => {
    if (cur.season == year) {
      acc[cur.id] = cur.id;
    }
    return acc;
  }, {});

  let oversperpalyer = {};

  let bowlersScore = playerRuns.reduce((acc, cur) => {
    if (mathidthisYear[cur.match_id]) {
      if (acc[cur.bowler]) {
        acc[cur.bowler] += parseInt(cur.total_runs);
     
      } else {
        acc[cur.bowler] = parseInt(cur.total_runs);
        oversperpalyer[cur.bowler] = 1;
      }

         if (oversperpalyer[cur.bowler]) {
          oversperpalyer[cur.bowler]++;
        }else{
          oversperpalyer[cur.bowler]=1;
        }
    }
    return acc;
  }, {});

  let averagerunsPerBowler = {};
  for (let val in oversperpalyer) {
    averagerunsPerBowler[val] = bowlersScore[val] / oversperpalyer[val] *6;
  }

  let array = Object.entries(averagerunsPerBowler);

  let sortedarray = array.sort((firstelement, secondelement) => {
    return secondelement[1] - firstelement[1];
  });
  let firsttenvalues = array.reverse().slice(0, 10);
  let toptenplayers = Object.fromEntries(firsttenvalues);

  return toptenplayers;
}
module.exports = topTenBowlers;
